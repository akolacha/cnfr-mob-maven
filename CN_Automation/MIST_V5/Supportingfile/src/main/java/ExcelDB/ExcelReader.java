package ExcelDB;


import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;

/**
 * Provides methods to access row and column information on a work book and sheet.
 * @author ShivaPrakash
 *
 */
public class ExcelReader extends AbstractExcelConnection {
	
	public ExcelReader( ) {
		super(false);
	}
	
	public ExcelReader(String filepath ) {
		super(false);
		try {
			open(filepath);
		} catch (IllegalArgumentException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the rows belonging to the given sheet in the workbook.
	 * @param sheetName
	 * @return Enumeration<Row>
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	public Enumeration<Row> getRows( String sheetName ) throws SQLException, IllegalArgumentException {
		if ( sheetName == null || sheetName.isEmpty() )
			throw new IllegalArgumentException("Sheet name cannot be empty");
		
		return executeQuery(String.format( "Select * from [%s$]", sheetName ));
	}
	
	/**
	 * Gets the rows belonging to the sheet in the work book. The filter is applied on the requested
	 * work sheet and the resulting rows are returned.
	 * @param sheetName
	 * @param filter
	 * @return Enumeration<Row>
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	public Enumeration<Row> getRows( String sheetName, String filter ) throws SQLException, IllegalArgumentException {
		if ( sheetName == null || sheetName.isEmpty( ) )
			throw new IllegalArgumentException("Sheet name cannot be empty");
		
		if( filter == null || filter.isEmpty( ) )
			throw new IllegalArgumentException("Filter cannot be empty");
		
		return executeQuery(String.format( "Select * from [%s$] where %s", sheetName, filter ));
	}
	
	/**
	 * Gets the rows from the work sheet that matches the specified filter conditions.
	 * @param sheetName
	 * @param filterValues
	 * @return Enumeration<Row>
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	public Enumeration<Row> getRows(String sheetName, HashMap<String,Object> filterValues ) throws SQLException, IllegalArgumentException {
		if ( sheetName == null || sheetName.isEmpty( ) )
			throw new IllegalArgumentException("Sheet name cannot be empty");
		
		if( filterValues.size() == 0 )
			throw new IllegalArgumentException("Filter values cannot be empty");

		// construct the query with filters appended using and clause.
		StringBuffer sqlBuffer = new StringBuffer( );
		sqlBuffer.append( String.format( "Select * from [%s$] where %s", sheetName, convertToFilterExpression(filterValues) ) );
		return executeQuery( sqlBuffer.toString( ) );
	}
}
