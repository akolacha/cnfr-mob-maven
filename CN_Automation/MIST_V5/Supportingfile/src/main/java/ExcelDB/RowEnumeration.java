package ExcelDB;


import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.NoSuchElementException;


/**
 * Provides an enumerable row implementation to navigate through the rows 
 * in the current result set. 
 * @author M1026473
 * 
 */
public class RowEnumeration implements Enumeration<Row> {

	ResultSet m_results;
	boolean m_canRead;
	int m_colCount = 0;
	
	public RowEnumeration( ResultSet results ) {
		m_results = results;
		try {
			ResultSetMetaData rsmd = m_results.getMetaData( );
			m_colCount = rsmd.getColumnCount();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns true, if the next element exists.
	 */
	@Override
	public boolean hasMoreElements() {
		try {
			m_canRead = m_results.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// close the result set when we are at the end of the cursor.
		try {
			if ( !m_canRead ){
				m_results.getStatement().close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return m_canRead;
	}

	/**
	 * Returns the next element when available, otherwise it throws NoSuchElement exception.
	 */
	@Override
	public Row nextElement() throws NoSuchElementException {
		if ( m_canRead ){
			try {
				return new Row( m_results, m_colCount );
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		throw new NoSuchElementException( );
	}
	

	
}
